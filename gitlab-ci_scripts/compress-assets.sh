
  #url=:https://docs.gitlab.com/ce/user/project/pages/introduction.html#serving-compressed-assets

  find public -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec gzip -f -k {} \;
